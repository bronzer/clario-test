import {URLS, UserInfo} from '../models';

export class MainApi {
    fetchData (): Promise<UserInfo[]> {
        return fetch(URLS.DATA)
            .then(response => response.json())
            .catch(err => console.log(err));
    }
}