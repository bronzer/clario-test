import React from 'react';
import {MainApi} from '../api/main.api';
import {UserInfo} from '../models';

const api = new MainApi();

export const useUserInfoFetcher = () => {
    const [data, setData] = React.useState<UserInfo[]>([]);

    React.useEffect(() => {
        api.fetchData()
            .then(responseData => {
                setData(responseData);
            });
    }, []);

    return data;
};