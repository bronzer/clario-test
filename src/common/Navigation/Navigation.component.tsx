import React from 'react';

import styles from './Navigation.module.scss';
import {Icon} from '../Icons/Icon.component';
import {ICONS} from '../Icons/Icons.collection';
import {NavLink} from 'react-router-dom';
import {PATHS} from '../../App.routes';

type NavigationProps = {};

const links = [{
    to: PATHS.ROOT,
    icon: ICONS.IconFolder,
}, {
    to: PATHS.TABLE,
    icon: ICONS.IconChart,
}];

export const Navigation = React.memo<NavigationProps>(props => {
    const linkCollection = links.map(link => (
        <NavLink
            key={link.to}
            to={link.to}
            exact={true}
            className={styles['app-navigation__link']}
            activeClassName={styles['app-navigation__link--active']}>
            <Icon icon={link.icon} />
        </NavLink>
    ));

    return (
        <div className={styles['app-navigation']}>
            <div className={styles['app-navigation__logo']}>
                <Icon icon={ICONS.IconShield} />
            </div>
            {linkCollection}
        </div>
    );
});
