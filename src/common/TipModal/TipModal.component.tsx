import React from 'react';
import styles from './TipModal.module.scss';
import {Icon} from '../Icons/Icon.component';
import {ICONS} from '../Icons/Icons.collection';

type TipModalProps = {
    onClose: () => void;
};

export const TipModal = React.memo<TipModalProps>(props => {
    return (
        <div className={styles['app-tip-modal']}>
            <span className={styles['app-tip-modal__icon']}>
                <Icon icon={ICONS.IconChart} />
            </span>
            Some tip
            <button type="button" className={styles['app-tip-modal__close-btn']} onClick={props.onClose}>Close</button>
        </div>
    );
});
