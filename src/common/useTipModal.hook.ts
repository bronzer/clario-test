import React from 'react';
import {getFromSessionStorage, setToSessionStorage} from '../utils/storage.utils';

const tipModalKey = 'tipShown';

export const useTipModal = (dependency: any) => {
    const [show, setShow] = React.useState(false);
    const item = getFromSessionStorage(tipModalKey);

    React.useEffect(() => {
        let id = 0;

        if (item === null) {
            id = window.setTimeout(() => {
                setShow(true);
                setToSessionStorage(tipModalKey, 'true');
            }, 5000);
        }

        return () => {
            clearTimeout(id);
        }
    }, [dependency, item]);

    return {show, setShow};
};
