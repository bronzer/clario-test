import React from 'react';

import classNames from 'classnames';

import styles from './Table.module.scss';
import {TableRow} from '../../models';

type TableProps = {
    labels: string[];
    tableRows: TableRow;
};

const headerClassname = classNames(styles['app-table__row'], styles['app-table__row--header']);

export const Table = React.memo<TableProps>(props => {
    const width = `${100 / props.labels.length}%`;
    const tableHeaderCells = props.labels.map(label =>
        <div key={label} className={styles['app-table__cell']} style={{width}}>{label}</div>,
    );
    const tableRowsElements = props.tableRows.map(row => {
        const cells = props.labels.map(label =>
            <div
                key={`${label}-${row.id}`}
                className={styles['app-table__cell']}
                style={{width}}>{row[label]}</div>
        );

        return <div key={row.id} className={styles['app-table__row']}>{cells}</div>
    });

    return (
        <div className={styles['app-table']}>
            <div className={headerClassname}>{tableHeaderCells}</div>
            {tableRowsElements}
        </div>
    );
});
