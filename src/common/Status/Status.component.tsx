import React from 'react';
import {STATUS} from '../../models';
import {capitalizeFirstLetter} from '../../utils/common';

import styles from './Status.module.scss';
import classNames from 'classnames';

type StatusProps = {
    status: STATUS
};

const getClassName = (status: STATUS) => {
    let additional;

    switch (status) {
        case STATUS.PAID:
            additional = styles['app-status--paid'];
            break;
        case STATUS.UNPAID:
            additional = styles['app-status--unpaid'];
            break;
        default:
            additional = styles['app-status--pending'];
    }

    return classNames(styles['app-status'], additional);
};

export const Status = React.memo<StatusProps>(props => {
    return (
        <div className={getClassName(props.status)}>
            { capitalizeFirstLetter(props.status) }
        </div>
    );
});
