import React from 'react';

import styles from './Header.module.scss';

type HeaderComponentProps = {
    username: string;
};

export const HeaderComponent = React.memo<HeaderComponentProps>(props => {
    return (
        <div className={styles['app-header']}>
            <div className={styles['app-header__user-info']}>
                <div className={styles['app-header__avatar']}>
                    <img src="https://vectorified.com/images/avatar-icon-png-9.jpg" alt="Avatar"/>
                </div>
                <span>{ props.username }</span>
            </div>
        </div>
    );
});
