import React from 'react';

import styles from './SectionLayout.module.scss';

type SectionLayoutProps = {
    children: React.ReactNode;
};

export const SectionLayout = React.memo<SectionLayoutProps>(props => {
    return (
        <div className={styles['app-section-layout']}>
            {props.children}
        </div>
    );
});
