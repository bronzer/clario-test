import React from 'react';

type IconProps = {
    icon: React.ElementType
};

export const Icon = React.memo<IconProps>(props => {
    const { icon: IconElement } = props;
    return (
        <i>
            <IconElement />
        </i>
    );
});
