import { IconFolder } from './IconFolder.svg';
import { IconChart } from './IconChart.svg';
import { IconShield } from './IconShield.svg';

export const ICONS = {
    IconFolder,
    IconChart,
    IconShield
};