import React from 'react';

import styles from './MainLayout.module.scss';

type MainLayoutProps = {
    header: React.ReactNode;
    sideBar: React.ReactNode;
    children: React.ReactNode;
};

export const MainLayout = React.memo<MainLayoutProps>(props => {
    return (
        <div className={styles['app-layout']}>
            <header className={styles['app-layout__header']}>
                { props.header }
            </header>
            <div className={styles['app-layout__sidebar']}>
                { props.sideBar }
            </div>
            <div className={styles['app-layout__content']}>
                { props.children }
            </div>
        </div>
    );
});
