import React from 'react';
import {BrowserRouter} from 'react-router-dom';
import {AppRoutes} from './App.routes';
import './App.css';
import {MainLayout} from './common/MainLayout/MainLayout.component';
import {Navigation} from './common/Navigation/Navigation.component';
import {HeaderComponent} from './common/Header/Header.component';

function App() {
  return (
      <BrowserRouter>
          <MainLayout header={<HeaderComponent username="Alex B" />} sideBar={<Navigation />}>
              <AppRoutes />
          </MainLayout>
      </BrowserRouter>
  );
}

export default App;
