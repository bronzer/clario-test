export const getFromSessionStorage = (key: string) => {
    return window.sessionStorage.getItem(key);
};

export const setToSessionStorage = (key: string, value: string) => {
    return window.sessionStorage.setItem(key, value);
};