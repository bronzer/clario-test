import React from 'react';
import { UserInfo} from '../models';
import {Status} from '../common/Status/Status.component';

type TableRowData = {
    id: string;
    name: string;
    status: React.ReactNode;
    total: string;
    totalRaw: number;
    [key: string]: string | number | React.ReactNode,
}

const MINUTES_IN_HOUR = 60;

const sortDates = (dates: string[]) =>
    dates.sort((a, b) => new Date(a).valueOf() - new Date(b).valueOf());

const sortByTotal = (tableRows: TableRowData[]) => {
    return tableRows.sort((a, b) => b.totalRaw - a.totalRaw);
};

const createLabels = (datesSorted: string[]) => {
    return ['name', ...datesSorted, 'status', 'total'];
};

const transformMinutes = (minutesRaw: number) => {
    const hours = (minutesRaw / MINUTES_IN_HOUR);
    const rhours = Math.floor(hours);
    const minutes = (hours - rhours) * MINUTES_IN_HOUR;
    const rminutes = Math.round(minutes);

    const hoursStr = rhours !== 0 ? `${rhours}h ` : '';
    const minutesStr = rminutes !== 0 ? `${rminutes}m` : '';

    return `${hoursStr}${minutesStr}`;
};

export const createUserTableData = (data: UserInfo[]) => {
    // set for removing duplicates
    const dates = new Set<string>();

    const tableRows = data.reduce<TableRowData[]>((result, user) => {
        const tableRowData: Partial<TableRowData> = {
            id: user.name,
            name: user.name,
            // don't know whether it's good, but it's better decision for current time
            status: <Status status={user.status} />,
        };

        const total = user.data.reduce<number>((total, data) => {
            tableRowData[data.date] = transformMinutes(data.minutes);
            dates.add(data.date);

            return total + data.minutes;
        }, 0);

        // totalRaw is number that will help to sort rows
        tableRowData.totalRaw = total;
        tableRowData.total = transformMinutes(total);

        return [...result, tableRowData as TableRowData];
    }, []);

    const datesSorted = sortDates(Array.from(dates));
    const tableRowsSorted = sortByTotal(tableRows);
    const labels = createLabels(datesSorted);

    return {labels, tableRows: tableRowsSorted};
};