import React from 'react';
import { Link } from 'react-router-dom';
import {PATHS} from '../../App.routes';

export const RootPage = React.memo(() => {
    return (
        <div>
            This is stub root page<br />
            <Link to={PATHS.TABLE}>Go to table</Link>
        </div>
    );
});
