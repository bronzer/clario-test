import React from 'react';
import {useUserInfoFetcher} from '../../common/useDataFetcher.hook';
import {createUserTableData} from '../../utils/table.utils';
import {Table} from '../../common/Table/Table.component';
import {useTipModal} from '../../common/useTipModal.hook';
import {TipModal} from '../../common/TipModal/TipModal.component';
import {SectionLayout} from '../../common/SectionLayout/SectionLayout.component';

export const TablePage = React.memo(() => {
    const data = useUserInfoFetcher();
    const tableData = createUserTableData(data);
    const {show: showTip, setShow} = useTipModal(data);
    const onTipClose = () => {
        setShow(false);
    };

    return (
        <div>
            <SectionLayout>
                <h3>Table</h3>
                <Table labels={tableData.labels} tableRows={tableData.tableRows} />
            </SectionLayout>
            {/*{showTip && <TipModal onClose={onTipClose}/>}*/}
            <SectionLayout>
                <TipModal onClose={onTipClose}/>
            </SectionLayout>
        </div>
    );
});
