const { createProxyMiddleware } = require('http-proxy-middleware');
module.exports = function(app) {
  app.use(
    '/test',
    createProxyMiddleware({
      target: 'https://d3qgrbb3ofqfjo.cloudfront.net',
      changeOrigin: true,
      logLevel: 'debug'
    })
  );
};