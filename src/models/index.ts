import React from 'react';

export enum URLS {
    DATA = '/test/api-response.json'
}

export enum STATUS {
    PAID = 'paid',
    UNPAID = 'unpaid',
    PENDING = 'pending',
}

export type UserData = {
    date: string,
    minutes: number,
}

export type UserInfo = {
    name: string,
    status: STATUS,
    data: UserData[]
}

export type TableRow = {
    id: string;
    [key: string]: string | number | React.ReactNode;
}[];