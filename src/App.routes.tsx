import React from 'react';
import { Route } from 'react-router-dom';
import {RootPage} from './components/Root/Root.page';
import {TablePage} from './components/TablePage/Table.page';

export enum PATHS {
    ROOT = '/',
    TABLE = '/table',
}

type RouteType = {
    page: React.ComponentType,
    path: PATHS;
    isExact?: boolean;
}

const routes: RouteType[] = [{
    page: RootPage,
    path: PATHS.ROOT,
    isExact: true,
}, {
    page: TablePage,
    path: PATHS.TABLE,
    isExact: true,
}];

export const AppRoutes = () => {
    const routeElements = routes.map(route =>
        <Route key={route.path} component={route.page} exact={route.isExact} path={route.path} />
    );

    return <>{ routeElements }</>
};
